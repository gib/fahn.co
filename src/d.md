---
title: Downloads
layout: base.liquid
data:
  nav: downloads
  downloads:
  - name: up-rs Linux
    filename: upl
  - name: up-rs Darwin
    filename: upd
---

{% comment %} Top level downloads page, a list of all downloads. {% endcomment %}

Short-links to downloads of tools I use for bootstrapping new machines.

## Links

<div class="blogs-list">
{% for download in page.data.downloads %}
<div class="blogs">
  <p class="blogs">
  <a class="blogs-subentry" href="./d/{{download.filename}}">{{download.name}}</a> (<a class="blogs-subentry" href="https://github.com/gibfahn/up-rs">GitHub Repo</a>)
  </p>
</div>
{% endfor %}
</div>
