---
title: Conventional Commits
published_date: "2020-05-19 15:42:49 +0000"
layout: default.liquid
is_draft: false
---
# Conventional Commits

[Conventional Commits][] are a way to put extra information in your commit messages for machines to
read. This has two major benefits:

1. Nice changelogs can be automatically generated.
2. The semver-ness of a release can be calculated, allowing automatic version bumping.

This means that if you use Conventional Commits and have fully-automated testing (no manual QA
process) you can fully automate tooling releases.

Note that this is primarily useful for tools and libraries that follow a semver-ish release format,
not for services where `master`is always live, such as [GitHub Flow][].

I won't go into the format in too much detail, as the [Conventional Commits][] docs do a good job of
that. Instead I'll just give an overview of the categories I've found through looking at major
projects that

## Commit Types

The ones I've seen recommended are:

- `feat`: a new feature
- `fix`: a bug fix
- `refactor`: a code change that neither fixes a bug nor adds a feature
- `docs`: documentation only changes
- `test`: adding missing tests or correcting existing tests
- `perf`: a code change that improves performance
- `chore`: updating grunt tasks etc; no production code change
- `style`: changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)
- `build`: changes that affect the build system or dependencies (example scopes: npm, maven, )
- `ci`: changes to our CI configuration files and scripts (example scopes: Travis, Circle, BrowserStack, SauceLabs)
- `revert`: revert of a previous commit

Note that a breaking change is not a type, it's a flag you add with a `!` e.g. `type(scope)!`, and
you add a `BREAKING CHANGE:` footer to note what the breakage is.


[GitHub Flow]: https://guides.github.com/introduction/flow/
[Conventional Commits]: https://www.conventionalcommits.org/
