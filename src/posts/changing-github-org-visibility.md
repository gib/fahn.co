---
title:   Changing GitHub Org Visibility
layout: default.liquid
published_date: 2017-09-04 20:13:19 +0100

data:
  heading: Changing GitHub Org Visibility
---

When you join a new organisation on GitHub, you get a bit of bling on your
profile page, in the form of an organisations section like this:

![github-orgs](../img/{{page.slug}}/github-orgs.png)

Then one day you look at your profile while logged out, and none of your
organisations are visible 😱. Turns out no-one else could see which orgs you're
a member of.

This is because GitHub org visibility is set to private by default, which means
no-one who isn't a member of that organisation can see that you are a member.

To fix this, whenever you're given access to an org (example links for `nodejs`):
- Go to the [org page][]
- Click [`People`][]
- [Search for your username][org search gibfahn]
- Change your `Visibility` from `Private` to `Public`

![github-org-visibility](../img/{{page.slug}}/github-org-visibility.png)

This works for GitHub Enterprise too.

[org page]: https://github.com/nodejs
[`People`]: https://github.com/orgs/nodejs/people
[org search gibfahn]: https://github.com/orgs/nodejs/people?utf8=%E2%9C%93&query=gibfahn
