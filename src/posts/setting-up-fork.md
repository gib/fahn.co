---
layout: default.liquid

title:   Setting up a Github fork
published_date: 2017-04-12 18:17:00 +0100

data:
  heading: Setting up a Github fork

---

If you're planning to contribute to an open-source project on Github, the first
step is to create a fork. There are a couple of things you can do to set
yourself up for success.

## Step 1: Fork and clone the repo

You will end up with an `up` remote, which is the upstream repo, and a `fork`
remote, which is your fork repo.

If you haven't already, set up [ssh keys][] and [gpg keys][]. To set up `hub`
see [here][Hub README].

```sh
# For public repos use https for `up` so you don't accidentally push.
git clone -o up https://github.com/nodejs/node.git && cd node
# This will add a remote called `fork` that points to your fork.
hub fork --remote-name=fork
```

#### Change the default branch

If you don't do this, sooner or later you'll do a `git checkout master`, and git
will check out a branch based on your fork's master, which will be behind,
meaning your PR will have the wrong commits etc. etc.

The best way to avoid this is to always delete all the branches on your fork,
and to always rename your remotes. You'll always want fork and upstream as
remotes anyway, so there's no downside. You'll need a placeholder branch to be
the `HEAD` branch of your remote, I use `oldmaster`.

```sh
# Create a new placeholder branch from the default branch for this repo.
git push fork @:refs/heads/oldmaster
```

Now go to your fork's page on Github and then `Settings`->`Branches`->`Default`
branch (URL should be similar to
[https://github.com/gibfahn/node/settings/branches][]). Change the default
branch to `oldmaster` (yes you understand the risks, it's your fork, so it
doesn't affect others).

#### Remove other branches and tags

Now remove all existing branches.

**N.B.** This deletes all branches and tags in the fork repo, don't run it on an existing repo.

```sh
# Delete branches.
git ls-remote --heads fork \
  | awk '!/refs\/heads\/oldmaster/ {print ":" $NF}' \
  | xargs -n 10 git push fork \
# Delete tags.
git tag -l | xargs -n 10 git push --delete fork
# Clean up.
git remote prune fork
```

This may take a while, but by the end you should have a nice clean repo. Check
by going to the Branches list on Github (e.g. [https://github.com/gibfahn/node/branches][]).

## Step 3: Create a branch and start hacking

Now you're good to go, you can just create a branch and start hacking on your
changes. If you don't know which branch to raise PRs against, it's probably `master`.

```sh
# Create a local branch based on up/master, try to use a meaningful name.
git checkout -b my-new-feature up/master
```

Once you've finished, to add your changes and commit them. Good commit messages
are very important, if your project has guidelines, follow them, otherwise take
inspiration from [here][Commit Messages 1] or [here][Commit Messages 2]. Also
use `git log up/master` to check the style for that repo.

```sh
git add my/changed/paths # Or `git add -A` to add everything.
git commit # Pick a clear message, follow project guidelines.
git push -u fork my-new-feature
hub pull-request
```

[Commit Messages 1]: https://chris.beams.io/posts/git-commit/
[Commit Messages 2]: http://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html
[Hub README]: https://github.com/github/hub#installation
[gpg keys]: ./setting-up-gpg-keys.html
[https://github.com/gibfahn/node/branches]: https://github.com/gibfahn/node/branches
[https://github.com/gibfahn/node/settings/branches]: https://github.com/gibfahn/node/settings/branches
[ssh keys]: ./setting-up-ssh-keys.html
