---
title: Finding subprocesses resetting the sudo timestamp
published_date: 2024-08-23 11:22:56.918561 +0000
layout: default.liquid
---

# Finding subprocesses resetting the sudo timestamp

I recently had to debug an issue where I was running several processes in an automation environment,
and one of them was running `sudo --reset-timestamp` and breaking the sudo auth I had given to the
tty for that session.

Debugging it wasn't immediately obvious, so documenting it for future reference.

## Setup

Create a fake sudo that logs what it is doing and who ordered it before running the actual sudo
command.

```sh
brew install pstree

path_dir=$TMPDIR/sudo-debug
mkdir -p "$path_dir"

echo >"$path_dir"/sudo '#!/bin/zsh
/opt/homebrew/bin/pstree -g 3 -p $$ >>$TMPDIR/sudo-log
echo >>$TMPDIR/sudo-log

exec /usr/bin/sudo $@
'

chmod +x "$path_dir"/sudo
```

## Run

Assuming your script is run as `/path/to/myscript args...` then you'd run something like:

```sh
rm $TMPDIR/sudo-log
PATH=$TMPDIR/sudo-debug:$PATH /path/to/myscript args...
```

Then looking at the `$TMPDIR/sudo-log` file would show you blocks like:

```
─┬= 00001 root /sbin/launchd
 └─┬= 25111 gib /Applications/kitty.app/Contents/MacOS/kitty
   └─┬= 99824 root /usr/bin/login -f -l -p gib /Applications/kitty.app/Contents/MacOS/kitten run-shell --shell /bin/zsh --shell-integration no-title --cwd=/Users/gib
     └─┬= 99825 gib -zsh
       └─┬= 99872 gib /path/to/myscript args...
         └─┬─ 07691 gib some-command some-args
           └─┬─ 07693 gib /bin/zsh /var/folders/k3/zn__xcl93gggy86h_80yj92w0000gn/T//sudo-debug/sudo --reset-timestamp
             └─┬─ 07694 gib /opt/homebrew/bin/pstree -g 3 -p 7693
               └─── 07695 root ps -axwwo user,pid,ppid,pgid,command

```

In this case the `some-command some-args` is the command that is running `sudo --reset-timestamp`.

## Fix

In this specific case Homebrew was resetting the timestamp, see [Homebrew/discussions#5528](https://github.com/orgs/Homebrew/discussions/5528) for the reasoning behind that change.

The solution (from that link) was to isolate brew commands to their own pseudo-tty to avoid the
timestamp reset affecting the rest of the current tty. This was done by adding this function to the
shell script that calls brew:

```sh
# Work around <https://github.com/orgs/Homebrew/discussions/5528>
brew() {
  script -q /dev/null "$(command -v brew)" "$@"
}
```

See the [commit](https://github.com/gibfahn/dot/commit/83e49543c4577bd33e114ec5a7a16378364458f3) for
a worked example.
