---
layout: default.liquid

title:   Terminal Tips
published_date:    2017-04-13 15:55:00 +0100

data:
  heading: Terminal Tips
---

This contains random assortment of things I've found useful. If you've wanted a
better way to do something, chances are someone else has already implemented it.

# Bash

## Utilities

### Finding files

There are a number of tools that try to improve on `grep`. [`ripgrep`][] is the
best one I've come across.

For searching files a fuzzy finder is pretty nice.

Either way it's worth learning the basics of `find` and `grep`, at least enough
to know when to use it (you can always Google for `find`s weird syntax).

### File watching

If you subscribe to the Unix way of doing things, you probably want a simple
tool that takes a list of files to watch, and runs an arbitrary command when
any of them change. [`entr`][] does exactly that.

##### Example usage:

```sh
# Run rust project when any non-gitignored files change.
# Clear screen before each run.
rg -l "" | entr -c cargo run

# If any matching files change, run `node changed_file`
ls test/parallel/test-os* | entr node /_
```


<!-- TODO(gib): `entr` -->

# Git tricks

### Referring to commits

There are lots of useful shortcuts for referring to commits, see [the git
docs][git revisions documentation] and [this great StackOverflow answer][git
commit-ish stackoverflow] answer for an in-depth review.

<table>
<thead> <tr> <th>Commit-ish/Tree-ish</th> <th>Examples</th> </tr> </thead>
<tbody>
<tr> <td><code>&lt;sha1&gt;</code></td> <td><code>dae86e1950b1277e545cee180551750029cfe735</code></td> </tr>
<tr> <td><code>&lt;describeOutput&gt;</code></td> <td><code>v1.7.4.2-679-g3bee7fb</code></td> </tr>
<tr> <td><code>&lt;refname&gt;</code></td> <td><code>master</code>, <code>heads/master</code>, <code>refs/heads/master</code></td> </tr>
<tr> <td><code>&lt;refname&gt;@{&lt;date&gt;}</code></td> <td><code>master@{yesterday}</code>, <code>HEAD@{5 minutes ago}</code></td> </tr>
<tr> <td><code>&lt;refname&gt;@{&lt;n&gt;}</code></td> <td><code>master@{1}</code></td> </tr>
<tr> <td><code>@{&lt;n&gt;}</code></td> <td><code>@{1}</code></td> </tr>
<tr> <td><code>@{-&lt;n&gt;}</code></td> <td><code>@{-1}</code></td> </tr>
<tr> <td><code>&lt;refname&gt;@{upstream}</code></td> <td><code>master@{upstream}</code>, <code>@{u}</code></td> </tr>
<tr> <td><code>&lt;rev&gt;^</code></td> <td><code>HEAD^</code>, <code>v1.5.1^0</code></td> </tr>
<tr> <td><code>&lt;rev&gt;~&lt;n&gt;</code></td> <td><code>master~3</code></td> </tr>
<tr> <td><code>&lt;rev&gt;^{&lt;type&gt;}</code></td> <td><code>v0.99.8^{commit}</code></td> </tr>
<tr> <td><code>&lt;rev&gt;^{}</code></td> <td><code>v0.99.8^{}</code></td> </tr>
<tr> <td><code>&lt;rev&gt;^{/&lt;text&gt;}</code></td> <td><code>HEAD^{/fix nasty bug}</code></td> </tr>
<tr> <td><code>:/&lt;text&gt;</code></td> <td><code>:/fix nasty bug</code></td> </tr>
</tbody></table>

<!-- TODO(gib): what's the proper html element for figure annotations? -->
<p class="annotation"> Above table cribbed from the StackOverflow answer. </p>

Some of the most convenient ones are:
<table>
<thead> <tr> <th>Short</th> <th>Equivalent</th> </tr> </thead>
<tbody>
<tr> <td><code>@</code>, <code>@~</code></td> <td><code>HEAD</code>, <code>HEAD~</code></td> </tr>
<tr> <td><code>:/some msg</code></td> <td>First parent that contains <code>some msg</code></td> </tr>
<tr> <td><code>branch</code>, <code>tag</code></td> <td>Head of <code>branch</code> or <code>tag</code></td> </tr>
<tr> <td><code>branch^{/some msg}</code></td> <td>Search <code>branch</code> for <code>some msg</code></td> </tr>
</tbody></table>


[git revisions documentation]: https://www.kernel.org/pub/software/scm/git/docs/gitrevisions.html#_specifying_revisions
[git commit-ish stackoverflow]: http://stackoverflow.com/a/23303550/2799191
[`entr`]: http://entrproject.org/
[`ripgrep`]: https://github.com/BurntSushi/ripgrep
