---
title: macOS Tricks
published_date: "2017-04-12 18:17:00 +0100"
is_draft: true
layout: default.liquid
data:
  heading: macOS Tricks
---

### Reset the services menu

Useful if the `System Preferences` -> `Keyboard` -> `Shortcuts` -> `Services` menu is out of date.

```sh
# `man pbs` for more info.
/System/Library/CoreServices/pbs -flush
```
