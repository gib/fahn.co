---
layout: default.liquid
is_draft: true

title: Setting up GitLab Pages
---

### Why GitLab Pages

So why would you want to use GitLab pages to host your website?

It's easy to use, and a great alternative to GitHub pages, which is usually the
first place people go to get free static site hosting.

GitLab has [an integrated CI and building process][GitLab CI], which makes it really easy to
set up and maintain a powerful automated build process.

<!-- TODO(gib): Picture of GitLab's CI. -->

It's also nice to use something that isn't GitHub. I use GitHub a lot, and it's
a great tool, but it's not perfect, and GitLab might actually be a better
overall fit for your workflow.

### Initial Setup

The goal here is to get a hello world webpage running on some URL.

#### Sign Up

First step is to [create an account][GitLab Sign Up] if you don't already have
one. I recommend enabling 2FA, add a mobile, an Authenticator App like [Google
Authenticator][], and a physical key like a [YubiKey][] if you can get one.

#### Create Project

Next you need to create a new personal project. I recommend making it public,
but unlike with GitHub that's not required.

- Go to [Create Project][]
- Choose `a`->`b` <!-- TODO(gib): Add options -->
- Pick whatever name you'd like. The URL you'll be serving to (which defaults to
  `<username>.gitlab.io`) is usually a good default.

#### Set up Git

You need to clone, add, commit, and push.

Get the URL of your project. I recommend using ssh rather than https, because
in my experience it's easier to use, especially if you work on multiple
machines. If you haven't set up ssh keys before, go check out [this
post][Setting Up SSH Keys].

For my site the ssh url is `git@gitlab.com:gib/fahn.co.git`, and the https url
(which you can visit in the browser to see the project) is
`https://gitlab.com/gib/fahn.co`.

You want to `cd` to the directory you keep your code projects in. If you don't
yet have a good directory structure, check out [this post][Setting up the
FileSystem].

```sh
# Wherever you keep your code:
cd ~/code
# Use your URL, the `.git` at the end is optional:
git clone git@gitlab.com:gib/fahn.co
```

### Custom Domains

Links:

https://gitlab.com/help/user/project/pages/getting_started_part_three.md#dns-txt-record


<!-- TODO(gib): Add links -->
[GitLab CI]:
[GitLab Sign Up]:
[Google Authenticator]:
[YubiKey]:
[Create Project]:
[Setting Up SSH Keys]: ./setting-up-ssh-keys.html
[Setting up the FileSystem]: ./setting-up-filesystem.html
