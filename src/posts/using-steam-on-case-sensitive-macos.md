---
title: Using Steam and Epic Games on case-sensitive macOS
published_date: "2020-02-01 12:44:21 +0000"
layout: default.liquid
is_draft: false
---

_**EDIT: (2020-03-08)**_ Updated to note the fix for Epic Games.

Steam and Epic have put off adding support for case-sensitive macOS filesystems for years, despite
supporting them on Linux. See the Steam [Support Page][] for details.

The first step to take is to [Contact Steam Support][] to let them know that you are having this
issue, if enough people raise this I assume it will get prioritised.

## Detecting Case-Sensitivity

You can tell if you have a case-sensitive filesystem (and are thus hitting this issue) by running:

```sh
diskutil info /
```

You should see the following lines in the output:

```
File System Personality:   Case-sensitive APFS
Type (Bundle):             apfs
Name (User Visible):       APFS (Case-sensitive)
```

If you see `HFS` instead of `APFS`, then you're using an old version of macOS and need to find a
different guide (or update your OS).

## Fixing the Issue

### Install Steam/Epic

First install and open 'Steam.app' or 'Epic Games Launcher.app', the easiest way is via
[Homebrew][]:

```sh
# Install Steam.app
brew install --cask steam
# Or if you're using Epic Games
brew install --cask epic-games

# Open it so it starts up and gets to a failing point:
open -a Steam
open -a 'Epic Games Launcher'
```

### Move to a case-sensitive volume

Then run the following commands in a Terminal. This will create a new case-insensitive volume called
"Steam" or "Epic" and move the relevant files into it.

Everything should work after this.

#### Steam

```sh
# Create a new case-insensitive volume called "Steam".
this_disk=$(/usr/libexec/PlistBuddy -c "print APFSContainerReference" =(diskutil info -plist /))
diskutil apfs addVolume "$this_disk" APFS Steam
# Move the installed app into it.
mv /Applications/Steam.app /Volumes/Steam/
# Make /Applications/Steam.app a symlink to the new location.
ln -s /Volumes/Steam/Steam.app /Applications/

# Create Steam's cache dir if it doesn't already exist.
mkdir -p "$HOME/Library/Application Support/Steam"
# Move it to the case-insensitive volume.
mv "$HOME/Library/Application Support/Steam" /Volumes/Steam/Library
# Symlink it from the old location so it's still found.
ln -s /Volumes/Steam/Library "$HOME/Library/Application Support/Steam"
```

#### Epic

```sh
# Create a new case-insensitive volume called "Epic".
this_disk=$(/usr/libexec/PlistBuddy -c "print APFSContainerReference" =(diskutil info -plist /))
diskutil apfs addVolume "$this_disk" APFS Epic
# Move the installed app into it.
mv "/Applications/Epic Games Launcher.app" /Volumes/Epic/
# Make "/Applications/Epic Games Launcher.app" a symlink to the new location.
ln -s "/Volumes/Epic/Epic Games Launcher.app" /Applications/

# Create Epic's cache dir if it doesn't already exist.
mkdir -p "$HOME/Library/Application Support/Epic"
# Move it to the case-insensitive volume.
mv "$HOME/Library/Application Support/Epic" /Volumes/Epic/Library
# Symlink it from the old location so it's still found.
ln -s /Volumes/Epic/Library "$HOME/Library/Application Support/Epic"
```

[Contact Steam Support]: https://help.steampowered.com/en/
[Homebrew]: https://brew.sh/
[Support Page]: https://support.steampowered.com/kb_article.php?ref=8601-RYPX-5789
