---
title: Vim Tricks
published_date: "2017-04-12 18:17:00 +0100"
is_draft: true
layout: default.liquid
data:
  heading: Vim Tricks
---

### Search all files in a tree:

```vim
" Add files to arg list:
:args **/*.py
" Run command on files in arg list:
:argdo %s/foo/bar/c

" Optional: print arg list
:args
```

http://vim.wikia.com/wiki/Search_and_replace_in_multiple_buffers#All_files_in_a_tree

# TODO(gib): Finish this, see also other TODOs.

![Title](../img/some.png)

### Run replace on multiple files

```vim
:arglist **/*.py
:argdo %s/foo/bar/c
```

### Search but put cursor at end of match

```vim
" Show help
:h search-offset

" Puts cursor at end of match
/foo/e
" Puts cursor 1 char after end of match
/foo/e+1
" Puts cursor at char before start of match:
/foo/s-1
```

### Run command and put output in quickfix window

```vim
:cex system("flake8")
```

[Commit Messages 1]: https://chris.beams.io/posts/git-commit/
