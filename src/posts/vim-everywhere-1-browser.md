---
title: Vim Everywhere Part 1
published_date: "2017-04-12 18:17:00 +0100"
is_draft: true
layout: default.liquid
data:
  heading: Vim in the Browser
---

Okay, so you're a convert to the cult of Vim, and now you want to use the
Keyboard, and more importantly vim mappings, everywhere.

# TODO(gib): Finish this, see also other TODOs.

## Step 1: Foo

Bar

![Title](../img/some.png)


[Commit Messages 1]: https://chris.beams.io/posts/git-commit/
[Commit Messages 2]: http://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html
[ssh keys]: ./setting-up-ssh-keys.html
[gpg keys]: ./setting-up-gpg-keys.html
