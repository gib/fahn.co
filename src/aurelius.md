---
title: Gibson Aurelius Fahnestock
layout: default.liquid
data:
  nav: about
  title: GAF
---

Quick update on the baby situation...

Our son Gibson Aurelius Fahnestock (a.k.a. Gibson 6 or 小G), was born at 2023-01-20 at 18:37 GMT,
weighing 3455g. Baby and mother are both well.

## Pictures

For more pictures see [this shared album](https://www.icloud.com/sharedalbum/#B14GWZuqDuFdbhS).

![Gibson Aurelius Fahnestock 5](./img/{{page.slug}}/gibson-aurelius-fahnestock-5.webp)
_Gibson 4 helping Gibson 6 get some much-needed rest._

![Gibson Aurelius Fahnestock 1](./img/{{page.slug}}/gibson-aurelius-fahnestock-1.webp)
_First 24 hours._


![Gibson Aurelius Fahnestock 2](./img/{{page.slug}}/gibson-aurelius-fahnestock-2.webp)
_Stuffed_

![Gibson Aurelius Fahnestock 3](./img/{{page.slug}}/gibson-aurelius-fahnestock-3.webp)
_Gibson 5 messing up a nappy change despite expert assistance._

![Gibson Aurelius Fahnestock 4](./img/{{page.slug}}/gibson-aurelius-fahnestock-4.webp)
_Gibson 5 pretending he's watching the baby rather than messing about on the internet._

(Pictures of the mother redacted because I am bad at taking pictures and she is bad at accepting my
bad pictures.)

## Timeline

For the curious...

- 2023-01-19
  - 15:15: waters broke, went to hospital, 1.5cm dilated
- 2023-01-20
  - 09:30: 2cm dilated, epidural and oxytocin drip started
    - mother had low blood pressure symptoms due to the epidural (nausea, ringing ears)
  - 18:00: still labouring, 6cm dilated
    - baby low down, not able to turn or get out
    - mother exhausted, shivering, barely cogent
    - emergency caesarean section chosen due to lack of progress and baby's position
  - 18:37: G born, immediately peed on the paediatrician
  - 19:05: c-section finished, no complications
- 2023-01-22
  - 13:00 mother and baby well enough to go home
