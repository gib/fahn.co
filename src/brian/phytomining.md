---
title: Chemistry - Phytomining
layout: default.liquid
data:
  nav: brian
  title: Phytomining
---

Phytomining, also known as Phytoextraction, is a process discovered after Phytoremediation - which means plants remove heavy metals from highly toxic or contaminated soil.

Phytomining, using plants which have high growth rate and biomass production, is the process of obtaining metals from plants which are good at absorbing heavy metals in the soil. Those plants are hyper-accumulators, meaning that they are capable of absorbing large amounts of heavy metal ions. Meanwhile, obtaining the metals, the burning of plant also generates biomass energy - which is a renewable energy resource that we don't often use due to its inefficiency.

## Process

For the plant to absorb the metals, the metals have to be a dissolved solution. The metal ions are taken up by the plant roots and may directly go through the roots' cell wall. Or, the plant will take this process as absorption of minerals and store it in the rhizosphere of a plant. The plant is efficient at absorbing metals such as Fe or Zn when they release phytosiderophores, which is a substance released by plants when they are deficient of Iron or Zinc, meaning they will increase their capacity of absorbing those substances.

![Chemical Reaction](https://www.researchgate.net/profile/Nicolaus-Wiren/publication/12248057/figure/fig3/AS:669325551759360@1536591055005/A-General-structure-of-phytosiderophores-and-assignment-of-pKa-values-B-intramolecular.png)

The metal is finally absorbed as the plants are burnt to ashes. Pouring acids into the ashes solidifies them. The copper, in this case, is then displaced with iron and eventually extracted as a pure metal.

![Ground cleanup with plants](https://static.dw.com/image/38887073_7.png)
