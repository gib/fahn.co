---
title: Brian@Fahn
layout: base.liquid
data:
  nav: brian
  pages:
  - title: Chemistry - Phytomining
    path: phytomining
    published_date: 2023-03-05
---

{% comment %} Top level page, a list of all Brian pages. {% endcomment %}

<div class="blogs-list">
{% for page in page.data.pages %}
<div class="blogs">
  {% if page contains "published_date" %} <p class="blogs-date">{{ page.published_date }}</p> {% endif %}
  <a class="blogs" href="./brian/{{ page.path }}.html">{{ page.title }}</a>
</div>
{% endfor %}
</div>
