---
title: Blog@Fahn
layout: base.liquid
data:
  nav: blog
---

{% comment %} Top level page, a list of all blogs. {% endcomment %}

<div class="blogs-list">
{% for post in collections.posts.pages %}
<div class="blogs">
  {% if post contains "published_date" %} <p class="blogs-date">{{ post.published_date | date: '%A %d %B %Y' }}</p> {% endif %}
  <a class="blogs" href="{{ post.permalink }}">{{ post.title }}</a>
</div>
{% endfor %}
</div>
