---
title: Mae Fahnestock
layout: default.liquid
data:
  nav: about
  title: MF
---

Third time's the charm...

Our daughter Mae Fahnestock (a.k.a. Meimei or 妹妹), was born at 2024-10-18 at 11:50 GMT,
weighing 3625g. Baby and mother are both well.

## Pictures

For pictures see [this shared album](https://www.icloud.com/sharedalbum/#B2FG4TcsmuOUWkV).
