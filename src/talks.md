---
title: ""
layout: base.liquid
data:
  nav: talks
  title: Talks@Fahn
---

<div class="talks">

  <h3>Node.js - What's Next</h3>
  <p>Index - February 21, 2018</p>

  <div class="talk">
    <iframe class="talk" width="560" height="315" src="https://www.youtube.com/embed/YTJAt9kWE9Y" frameborder="0" allowfullscreen></iframe>
    <iframe class="talk" src="https://www.slideshare.net/slideshow/embed_code/key/e2sJKu86EEWruN" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" allowfullscreen> </iframe>
  </div>

  <br/>

  <h3>How Build Infrastructure Powers the Node.js Foundation</h3>
  <p>Node.js Interactive - October 4-6, 2017</p>

  <div class="talk">
    <iframe class="talk" width="560" height="315" src="https://www.youtube.com/embed/D09JaueQRcg" frameborder="0" allowfullscreen></iframe>
    <iframe class="talk" src="https://www.slideshare.net/slideshow/embed_code/key/jO7tsdQ9aD9TE3" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" allowfullscreen> </iframe>
  </div>
</div>
