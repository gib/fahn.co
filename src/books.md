---
title: Books
layout: base.liquid
data:
  nav: books
  books:
  - author: Stella Soulioti
    title: Sensations and Impressions
    filename: Stella_Soulioti_-_Sensations_and_Impressions
    date: 1937
    languages:
    - Chinese
  - author: Stella Soulioti
    title: Seven Stories for Children
    filename: Stella_Soulioti_-_Seven_Stories_for_Children
    date: 1954
    languages:
    - Chinese
---

{% comment %} Top level books page, a list of all books. {% endcomment %}

<div class="blogs-list">
{% for book in page.data.books %}
<div class="blogs">
  {% if book contains "date" %} <p class="blogs-date">{{ book.date }}</p> {% endif %}
  <p class="blogs">{{ book.author }} - {{ book.title }}</p>
  <p class="blogs-subentry">
    English: <a class="blogs-subentry" href="./books/{{book.filename}}.pdf">PDF</a> · <a class="blogs-subentry" href="./books/{{book.filename}}.txt">Plain Text</a>
  </p>

  {% for language in book.languages %}
  <p class="blogs-subentry">
    {{ language }}: <a class="blogs-subentry" href="./books/{{book.filename}}-{{language}}.txt">Plain Text</a>
  </p>
  {% endfor %}

</div>
{% endfor %}
</div>
