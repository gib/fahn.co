---
title: "Gibson Fahnestock"
layout: base.liquid
description: "Gibson Fahnestock's Personal Site"
data:
  nav: about
  title: Fahn.co
---

<br/>

<div class="logos">
  <a class="logos" href="https://github.com/gibfahn"> {% include "svg/github.svg" %} </a>
  <a class="logos" href="https://gitlab.com/gib"> {% include "svg/gitlab.svg" %} </a>
  <a class="logos" href="https://twitter.com/gibfahn"> {% include "svg/twitter.svg" %} </a>
  <a class="logos" href="https://www.linkedin.com/in/gibfahn"> {% include "svg/linkedin.svg" %} </a>
  <a class="logos" href="https://stackoverflow.com/users/2799191/gib"> {% include "svg/stackoverflow.svg" %} </a>
</div>
