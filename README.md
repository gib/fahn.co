Uses cobalt.rs under the covers.

### Developing

To install dependencies get rust from [here][rustup], then install [cobalt][] with:

```sh
cargo install cobalt-bin
```

To rebuild/serve site on save:

```sh
cobalt serve --drafts
```

### Adding Posts

```shell
cobalt new -f src/posts/ "<Post Title>"
cobalt serve --drafts

cobalt publish src/posts/<post-title>.md
# Remove the date (I should fix this at some point).
mv src/posts/<date><post-title>.md src/posts/<post-title>.md
```

Then add, commit, push.

### Generating the svg

Favicon font: Diogene Champignon.

[rustup]: https://rustup.rs/
[cobalt]: https://cobalt-org.github.io/
