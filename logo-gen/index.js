const child_process = require('child_process');
const fs = require('fs');
const path = require('path');

// const font = path.join(__dirname, 'fonts/Champignon.ttf');
const font = path.join(__dirname, 'fonts/Champignon_3.otf');

const TextToSVG = require('text-to-svg');
const textToSVG = TextToSVG.loadSync(font);

const attributes = {
  fill: 'black', // Edge of line colour.
  stroke: 'black', // Line fill colour.
};
const options = {x: 0, y: 0, fontSize: 72, anchor: 'top', attributes: attributes};

const svg = textToSVG.getSVG('Fahn.co', options);

const outDir = path.join(__dirname, 'out');

try {
  fs.mkdirSync(outDir);
} catch (e) {
  if (e.code !== 'EEXIST') throw e;
}

const svgPath = path.join(outDir, 'logo-fahn-co.svg');

fs.writeFileSync(svgPath, svg);

child_process.execSync(`open ${svgPath}`);

// console.log(svg);
