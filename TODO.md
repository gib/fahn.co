## TODO

## Blog ideas

- Mail.app message links (`~/bin/mail_link` and Karabiner).

- Remapping the Keyboard:
  - Ergodox
    - Ergodox Configurator
  - macOS
    - Karabiner Elements
  - Linux
    - xkb remapping (WIP)
  - Windows
    - AutoHotkey
- Vim in the browser
  - Vimium (SurfingKeys)
    - f
    - Remapping
    - GhostText (WIP)
- Keyboard Everywhere: Slack
- Easier Cmd-Tab (HyperSwitch)
- Getting started with a Unix machine master-page
  - Links to all the git/bash/etc pages

- Browser Plugins
  - LinkClump
  - Open all in tabs / Refined Github
  - Link to Vimium
